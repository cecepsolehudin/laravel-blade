<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function () {
        return view('welcome');
    });
    
    Route::get('table', function () {
        return view('table');
    });
    
    Route::get('data-table', function () {
        return view('data-table');
    });
    
    Route::get('/cast', 'CastController@index');
    Route::get('/cast/create', 'CastController@create');
    Route::post('/cast', 'CastController@store');
    Route::get('/cast/{cast_id}', 'CastController@show');
    Route::get('/cast/{cast_id}/edit', 'CastController@edit');
    Route::put('/cast/{cast_id}', 'CastController@update');
    Route::delete('/cast/{cast_id}', 'CastController@destroy');
    
    Route::get('/genre', 'GenreController@index');
    Route::get('/genre/create', 'GenreController@create');
    Route::post('/genre', 'GenreController@store');
    Route::get('/genre/{cast_id}', 'GenreController@show');
    Route::get('/genre/{cast_id}/edit', 'GenreController@edit');
    Route::put('/genre/{cast_id}', 'GenreController@update');
    Route::delete('/genre/{cast_id}', 'GenreController@destroy');
});

Route::resource('film', 'FilmController');

Route::resource('profile', 'ProfileController')->only([
    'index', 'update'
]);

Auth::routes();
