@extends('layout.master')
@section('judul')
    Halaman List Film
@endsection
@section('content')

@auth
    <a href="/film/create" class="btn btn-success my-2">Tambah Film</a>    
@endauth


<div class="row">
    @foreach ($film as $item)
        <div class="card mx-1" style="width: 18rem;">
            <img src="{{asset('img/' . $item->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
            <h5 class="card-title">{{$item->judul}} ({{$item->tahun}})</h5>
            <p class="card-text">{{Str::limit($item->ringkasan, 50)}}</p>
            @auth
                <form action="/film/{{$item->id}}" method="POST">
                    @method('delete')
                    @csrf
                    <a href="/film/{{$item->id}}" class="btn btn-primary">Read More</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-info">Edit</a>
                    <input type="submit" class="btn btn-danger" value="Delete">
                </form>
            @endauth
            @guest
                <a href="/film/{{$item->id}}" class="btn btn-primary">Read More</a>
            @endguest
          
            <br>
            <small>{{$item->created_at}}</small>
            </div>
        </div>
    @endforeach 
</div>



@endsection