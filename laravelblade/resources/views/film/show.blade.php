@extends('layout.master')
@section('judul')
    Halaman List Film
@endsection
@section('content')

<div class="card mx-1">
    <img src="{{asset('img/' . $film->poster)}}" width="50%" alt="...">
        <div class="card-body">
        <h5 class="card-title">{{$film->judul}} ({{$film->tahun}})</h5>
        <p class="card-text">{{$film->ringkasan}}</p>
        <small>{{$film->created_at}}</small><br>
        <a href="/film" class="btn btn-primary">Kembali</a>
    </div>
</div>

@endsection