@extends('layout.master')
@section('judul')
    Halaman Update Profile
@endsection
@section('content')
<form action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control" value="{{$profile->user->email}}" id="email" disabled>
    </div>
    <div class="form-group">
        <label>name</label>
        <input type="text" class="form-control" value="{{$profile->user->name}}" id="name" disabled>
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" class="form-control" name="umur" value="{{$profile->umur}}" id="umur" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" name="bio" id="bio" placeholder="Masukkan Bio" cols="30" rows="10">{{$profile->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="alamat">Alamat</label>
        <input type="text" class="form-control" name="alamat" value="{{$profile->alamat}}" id="alamat" placeholder="Masukkan Alamat">
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection