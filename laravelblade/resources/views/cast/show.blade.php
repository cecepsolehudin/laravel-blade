@extends('layout.master')
@section('judul')
    Halaman Detail Cast {{$cast->id}}
@endsection
@section('content')

<h3 class="text-primary">{{$cast->nama}}</h3>
<small>{{$cast->umur}} years old</small>
<p>{{$cast->bio}}</p>

@endsection