<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use Auth; 

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('profile.index', compact('profile'));
    }

    public function update(Request $request, $id){
        $this->validate($request,[
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
        ]);

        $profile_data = [
            'umur' => $request->umur,
            'bio' => $request->bio,
            'alamat' => $request->alamat,
        ];

        Profile::whereId($id)->update($profile_data);

        return redirect('/profile');
    }
}
